/* 
@Author <Cognizant>
@name <QuarterlyRefreshOpt>
@CreateDate <07/09/2016>
@Description <This class is for QuarterlyRefresh by Option> 
@Version <1.0>
*/
public class QuarterlyRefreshOpt{
    public Business_Project_Name__c bpObj{get;set;}
    public ProjectSelection__c psObj{get;set;}
    public ProjectSelection__c pssObj{get;set;}
    public list<wrapperClass> wrapperList{get;set;}
    public list<wrapperPSClass> wrapperpsList{get;set;}
    public Boolean varPrjSel{get;set;}
    public Boolean bpSel{get;set;}
    public Boolean bpSelUpdte{get;set;}
    public Boolean bpSelUpdte2{get;set;}
    public Boolean psSel{get;set;}
    public Boolean bpBol{get;set;}
    public Boolean bpBolupdate{get;set;}
    public Boolean buttonps{get;set;}
    public Boolean buttonps2{get;set;}
    public Boolean bolError{get;set;}
    public Map<Id,boolean> bpMap{get;set;}
    public Integer counter; 
    public Set<id> opptyid;
    public  List<Business_Project_Name__c> bpList;
    public Boolean qrCycle{get;set;}    
    public QuarterlyRefreshOpt(){
        
    }
    
    
    public QuarterlyRefreshOpt(ApexPages.StandardController controller) {
        system.debug('test by bitbuckt');
        bpObj = new Business_Project_Name__c();
        psObj = new ProjectSelection__c ();
        counter = 0;
        bpBol= true;
        buttonps2= true;
        
        bpMap = new Map<Id,boolean>();
        opptyid = new Set<id>();
        wrapperList = new list<wrapperClass>();
        wrapperpsList = new list<wrapperPSClass>();        
        wrapperClass bpWrap = new wrapperClass(new Business_Project_Name__c());
        wrapperPSClass psWrap = new wrapperPSClass(new ProjectSelection__c ());         
        counter++;
        bpWrap.counterWrap = counter;
        psWrap.counterWrap = counter;      
        wrapperList.add(bpWrap);  
        wrapperpsList.add(psWrap);
    }
    /*=================================Save Method==========*/    
    public PageReference save(){
        
        list<Business_Project_Name__c> bpList;
        Account acc ;
        Opportunity opp; 
        Integer i =0;
        bpList = new list<Business_Project_Name__c>();    
        
        bpBol = false;
        bpSel = true;     
        bpSelUpdte = true; 
        bpSelUpdte2 = false; 
        
        try{
            if(!wrapperList.isEmpty()){
                Set<id> oppId = new Set<Id>(); 
                for(wrapperClass Wrapper:wrapperList){
                    // if(oppId !=null && ! oppId.contains(Wrapper.wrapbpObj.Opportunity__c))
                    oppId.add(Wrapper.wrapbpObj.Opportunity__c);  
                    system.debug('--opptyid.-'+opptyid.size());          
                    if(opptyid.contains(Wrapper.wrapbpObj.Opportunity__c) ){
                        system.debug('--inside--');
                        bolError = true;   
                        break;
                    }
                    
                    
                    system.debug('--oppId--'+oppId);
                    acc = new Account(id=Wrapper.wrapbpObj.Account__c);
                    acc = [select id,Region__c from account where id=: Wrapper.wrapbpObj.Account__c];
                    if(Wrapper.wrapbpObj.Status__c == null){
                        Wrapper.wrapbpObj.Status__c ='None';         
                    }
                    opp = new Opportunity(id=Wrapper.wrapbpObj.Opportunity__c);
                    opp = [select id,Product_Line__c from opportunity where id=: Wrapper.wrapbpObj.Opportunity__c];
                    system.debug('---Account Id---'+Wrapper.wrapbpObj.Account__c);
                    system.debug('---Opportunity Id---'+Wrapper.wrapbpObj.Opportunity__c); 
                    system.debug('---Account Region---'+acc.Region__c);
                    system.debug('---Sales_Geography__c---'+opp.Product_Line__c);                
                    Wrapper.WrapbpObj.Sales_Geography__c = acc.Region__c;
                    Wrapper.WrapbpObj.Brand__c = opp.Product_Line__c;
                    system.debug('---Sales_Geography__c---'+ Wrapper.WrapbpObj.Sales_Geography__c );
                    system.debug('---Sales_Brand__c---'+Wrapper.WrapbpObj.Brand__c);    
                    // Wrapper.bolwrap = false; 
                    
                    bpList.add(Wrapper.WrapbpObj);
                    
                }
            }
            
            if(!bpList.isEmpty()){
                // bpList[0].Sales_Geography__c = acc.Region__c;
                // bpList[0].Brand__c = acc.Region__c;               
                upsert bpList;
            }
            system.debug('---bpList[0].id--'+bpList[0].id);
            bpMap.put(bpList[0].id,false);
            opptyid.add(bpList[0].Opportunity__c);
            
        }Catch(Exception Ex){    
        }
        return null;
    }
    public class wrapperClass{
        
        public Business_Project_Name__c wrapbpObj{get;set;}
        public Integer counterWrap{get;set;}
        public Boolean bolWrap{get;set;}
        
        public wrapperClass(Business_Project_Name__c WrapbpObj ) {
            
            this.wrapbpObj=WrapbpObj ;     
            
        }                      
    }
    
    public PageReference addRow(){
        wrapperClass bpWrap = new wrapperClass(new Business_Project_Name__c()); 
        bpSel = true;
        bpBol = true;
        
        system.debug('--bpBol--'+bpBol);
        counter++;
        bpWrap.counterWrap = counter; 
        wrapperList.add(bpWrap); 
        return null;            
        
    } 
    
    public PageReference removingRow(){
        
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        
        for(Integer i=0;i<wrapperList.size();i++){
            if(wrapperList[i].counterWrap == param ){
                wrapperList.remove(i);     
            }
        }       
        counter--;
        return null;    
    }
    
    public PageReference updateRow(){
        
        bpBolupdate = false; 
        bpSelUpdte = true;   
        bpSelUpdte2 = false;   
        
        
        List<Business_Project_Name__c> bpList = new list<Business_Project_Name__c>() ;
        for(wrapperClass Wrapper:wrapperList){
            bpList.add(Wrapper.WrapbpObj); 
            wrapper.bolWrap = false;   
        }
        update bpList;     
        return null;    
    }
    
    
    public PageReference updateRowSave(){
        
        bpBolupdate = true;    
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        List<Business_Project_Name__c> bpList = new list<Business_Project_Name__c>() ;
        for(Integer i=0;i<wrapperList.size();i++){
            
            if(wrapperList[i].counterWrap == param ){      
                bpList.add(wrapperList[i].WrapbpObj); 
                system.debug('---wrapperList[i].WrapbpObj--'+wrapperList[i].WrapbpObj.id);
                system.debug('---bpMap--'+bpMap);
                bpSelUpdte2 = bpMap.get(wrapperList[i].wrapbpObj.id);   
                bpSelUpdte = false;
                bpMap.put(wrapperList[i].wrapbpObj.id,true);
                wrapperList[i].bolWrap = true; 
                
            }       
            system.debug('--wrapperList-'+wrapperList);
            
        }
        return null; 
    }
    public class wrapperPSClass{
        
        public ProjectSelection__c wrappsObj{get;set;}
        public Integer counterWrap{get;set;}
        public Boolean selected {get; set;}
        public wrapperPSClass(ProjectSelection__c  WrappsObj ) {
            
            this.wrappsObj=WrappsObj ;  
            selected = false;   
            
        }                      
    }    
    
    public void saveProjectSelection(){
        
        Business_Project_Name__c bpObj = new Business_Project_Name__c();   
        bpObj = [select Account__c,Brand__c from Business_Project_Name__c where id =: wrapperpsList[0].wrappsObj.Business_Project__c];
        list<ProjectSelection__c> psList = new list<ProjectSelection__c>();
        psSel = true;
        buttonps = true;
        buttonps2=false;
        system.debug('--wrapperpsList-'+wrapperpsList);
        
        
        if(!wrapperpsList.isEmpty()){
            for(wrapperPSClass Wrapper:wrapperpsList){
                
                Wrapper.wrappsObj.Account__c = bpObj.Account__c;
                Wrapper.wrappsObj.Brand__c   = bpObj.Brand__c ;
                system.debug('--psObj.Project_Cycle__c-'+psObj.Project_Cycle__c);
                Wrapper.wrappsObj.Project_Cycle__c = psObj.Project_Cycle__c ;
                
                psList.add(Wrapper.wrappsObj);
            }
        }
        system.debug('--psList-'+psList);
        if(!psList.isEmpty()){
            // bpList[0].Sales_Geography__c = acc.Region__c;
            // bpList[0].Brand__c = acc.Region__c; 
            
            upsert psList;
        }        
        ProjectSelection__c  psObj = new ProjectSelection__c ();
        pssObj =  [SELECT status__C from ProjectSelection__c where id =: psList[0].id];
        
    }     
    
    
    public PageReference addRowPs(){
        wrapperPSClass psWrap = new wrapperPSClass (new ProjectSelection__c ()); 
        // buttonps = true;
        buttonps2=true;
        counter++;
        psWrap.counterWrap = counter; 
        wrapperpsList.add(psWrap ); 
        return null;                        
    } 
    
    public PageReference removingRowPs(){
        
        Integer param1 = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        
        for(Integer i=0;i<wrapperpsList.size();i++){
            if(wrapperpsList[i].counterWrap == param1 ){
                wrapperpsList.remove(i);     
            }
        }       
        counter--;
        return null;    
    }  
    
    public void saveQRCycle(){
        
        Business_Project_Name__c bpObj = new Business_Project_Name__c();   
        bpObj = [select Account__c,Brand__c from Business_Project_Name__c where id =: psObj.Business_Project__c];
        
        psObj.Account__c = bpObj.Account__c;
        psObj.Brand__c = bpObj.Brand__c ;
        qrCycle = true;
        
        insert psObj;   
    }    
}