@isTest
public class QuoteSelectionsControllerTest
{
   static testMethod void Test1(){
    
        SPSystems__Product_Category__c prodCategory = new SPSystems__Product_Category__c();
        prodCategory.name = 'Sample1';
        insert prodCategory;
        
        Product2 prod = new Product2();
        prod.Name = 'TEST PROD' ;
        prod.IsActive = true;
        prod.SPSystems__Product_Category__c = prodCategory.id;
        insert prod;
        
        SPSystems__Price_Set__c priceset = new SPSystems__Price_Set__c();
        priceset.name = 'Test1';
        priceset.SPSystems__Active__c = true;
        priceset.SPSystems__Product_Category__c = prodCategory.id;
        insert priceset;
        
        SPSystems__Price_Set_Item__c pricesetitems = new SPSystems__Price_Set_Item__c();
        pricesetitems.SPSystems__Active__c = true;
        pricesetitems.SPSystems__Item_Type__c = 'Standard';
        pricesetitems.SPSystems__List_Price__c = 200;
        pricesetitems.SPSystems__Price_Set__c = priceset.id;
        pricesetitems.SPSystems__Product__c = prod.id;
        insert pricesetitems;
        
        SPSystems__Quote__c testQuote = new SPSystems__Quote__c();
        testQuote.name = 'First1';
        testQuote.SPSystems__Price_Set__c = priceset.id;
        insert testQuote;
    
         SPSystems__Price_Set__c priceset1 = new SPSystems__Price_Set__c();
        priceset1.name = 'Test2';
        priceset1.SPSystems__Active__c = true;
        priceset1.SPSystems__Product_Category__c = prodCategory.id;
        insert priceset1;
        
        test.starttest();
        ApexPages.StandardController sc = new ApexPages.standardController(testQuote);
    
        QuoteSelectionsController QuoteSelection1=new QuoteSelectionsController(sc);
        QuoteSelection1.priceBookCheck();
        QuoteSelection1.toSelect=pricesetitems.id;
        QuoteSelection1.addToShoppingCart();
        QuoteSelection1.toUnselect=pricesetitems.id;
        QuoteSelection1.removeFromShoppingCart();
        QuoteSelection1.toSelect=pricesetitems.id;
        QuoteSelection1.addToShoppingCart();
        test.stoptest();
   }
   
   static testMethod void Test2(){
    
        SPSystems__Product_Category__c prodCategory = new SPSystems__Product_Category__c();
        prodCategory.name = 'Sample1';
        insert prodCategory;
        
        Product2 prod = new Product2();
        prod.Name = 'TEST PROD' ;
        prod.IsActive = true;
        prod.SPSystems__Product_Category__c = prodCategory.id;
        insert prod;
        
        SPSystems__Price_Set__c priceset = new SPSystems__Price_Set__c();
        priceset.name = 'Test1';
        priceset.SPSystems__Active__c = true;
        priceset.SPSystems__Product_Category__c = prodCategory.id;
        insert priceset;
        
        SPSystems__Price_Set_Item__c pricesetitems = new SPSystems__Price_Set_Item__c();
        pricesetitems.SPSystems__Active__c = true;
        pricesetitems.SPSystems__Item_Type__c = 'Standard';
        pricesetitems.SPSystems__List_Price__c = 200;
        pricesetitems.SPSystems__Price_Set__c = priceset.id;
        pricesetitems.SPSystems__Product__c = prod.id;
        insert pricesetitems;
        
        SPSystems__Quote__c testQuote = new SPSystems__Quote__c();
        testQuote.name = 'First1';
        //testQuote.SPSystems__Price_Set__c = priceset.id;
        insert testQuote;
        
        test.starttest();
        ApexPages.StandardController sc = new ApexPages.standardController(testQuote);
    
        QuoteSelectionsController QuoteSelection1=new QuoteSelectionsController(sc);
        QuoteSelection1.onCancel();
        QuoteSelection1.onsave();
        QuoteSelection1.addToShoppingCart();
        QuoteSelection1.toUnselect=pricesetitems.id;
        QuoteSelection1.removeFromShoppingCart();
        test.stoptest();
   }
   
}