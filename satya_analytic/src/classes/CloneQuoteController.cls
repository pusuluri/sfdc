public with sharing class CloneQuoteController {

public Id QuoteId{get;set;}
List<SBQQ__Quote__c > ListOfQuote;

    public CloneQuoteController(ApexPages.StandardController controller) {
String strQry;
    QuoteId = apexpages.currentpage().getparameters().get('id');
     if (QuoteId!=null){
     strQry  = 'Select ' + getFieldList(SBQQ__Quote__c.getSObjectType())+ ' from SBQQ__Quote__c where';  
    strQry +=' id =';
               strQry +='\'';
               strQry += QuoteId;
               strQry +='\'';                         
      }
           
     ListOfQuote = Database.query(strQry);
    
    }
    
    public void clonequote(){
    
        SBQQ__Quote__c objNewProp = new SBQQ__Quote__c();  
        list <SBQQ__Quote__c> objNewProps = new list<SBQQ__Quote__c>();  
        if (ListOfQuote.size()>0 )
        {                        
            //objNewProp   = ListOfQuote[0].clone(true,true,false,false); 
            objNewProps.addAll(ListOfQuote.deepClone(true,true,true));
             insert objNewProps;
          }  
        
    }
    
    public static string getFieldList(Schema.SObjectType SObjTyp) {
        string qryStr = '';
        Schema.DescribesObjectResult metadata = SObjTyp.getDescribe();
        
        //set<string> excludeFieldList = getQuoteNonQueryFields();
        
        for (Schema.SObjectField field : metadata.fields.getMap().values()) {
            Schema.DescribeFieldResult fldMetadata = field.getDescribe();
            //if(!excludeFieldList.contains(fldMetadata.getName().toLowerCase())){
                if (qryStr != '')qryStr += ',';
                qryStr += fldMetadata.getName();
            //}
        }
        return qryStr;
    }

}