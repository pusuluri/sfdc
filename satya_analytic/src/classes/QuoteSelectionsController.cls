public with sharing class QuoteSelectionsController {

    public Quote__c theQuote {get;set;}
    public String searchString {get;set;}
    public Quote_Line_Item__c[] shoppingCart {get;set;}
    public Price_Set_Item__c[] AvailableProducts {get;set;}
    public Price_Set__c theBook {get;set;}   
    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    
    public Boolean overLimit {get;set;}
    
    private Boolean forcePricebookSelection = false;
    
    private Quote_Line_Item__c[] forDeletion = new Quote_Line_Item__c[]{};

     public static List<ProdTreeWrapper> Prods;
    
    public class ProdTreeWrapper{
          
          //parent Products
          public Product_Relationship__c grandparent {get;set;}
          //child Products
          public List<Product_Relationship__c> childsProds {get; set;}
          public string gparentname {get; set;}
          public Boolean selected {get; set;}
          public Boolean IsaddedtoCart {get; set;}
          
          
          public ProdTreeWrapper(){
          }
          
          //constructor     
          public ProdTreeWrapper(Product_Relationship__c parentProd, List<Product_Relationship__c> childProdNew){
               childsProds= childProdNew;
               gparentname=parentProd.name;
               grandparent= parentProd;
               selected = false;
               
           }
     } 

    public QuoteSelectionsController(ApexPages.StandardController controller) {

        
            theQuote = [select Id, Price_Set__c, Price_Set__r.Name from Quote__c where Id = :controller.getRecord().Id limit 1];
        
        // If products were previously selected need to put them in the "selected products" section to start with
        shoppingCart = [select Id,Discount__c, Product__c,Product__r.Name,Product__r.Description,Quantity__c, Total_Price__c, Unit_Price__c, Description__c from Quote_Line_Item__c where Quote__c=:theQuote.Id];

        // Check if Opp has a pricebook associated yet
        if(theQuote.Price_Set__c == null){
            Price_Set__c[] activepbs = [select Id, Name from Price_Set__c where Active__c = true limit 2];
            if(activepbs.size() == 2){
                forcePricebookSelection = true;
                theBook = new Price_Set__c();
            }
            else{
                theBook = activepbs[0];
            }
        }
        else{
            Price_Set__c[] activepbs1 = [select Id, Name from Price_Set__c where Active__c = true and id=:theQuote.Price_Set__c limit 1];
            if (activepbs1.size() > 0)
               theBook = activepbs1[0];
        }
        
        if(!forcePricebookSelection)
            updateAvailableList();
    }
    
    // this is the 'action' method on the page
    public PageReference priceBookCheck(){
    
        // if the user needs to select a pricebook before we proceed we send them to standard pricebook selection screen
        if(forcePricebookSelection){        
            return null;//changePricebook();
        }
       /* else{
        
            //if there is only one active pricebook we go with it and save the opp
            if(theQuote.Price_Set__r.Id != theBook.Id){
                try{
                    theQuote.Price_Set__r.Id = theBook.Id;
                    update(theQuote);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
        }*/
        return null;
    }
       

    public void updateAvailableList() {
    
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Price_Set__c, Active__c, Product__c, Product__r.Name, Product__r.Family, Product__r.IsActive, Product__r.Description, List_Price__c from Price_Set_Item__c where Active__c=true and Price_Set__c = \'' + theBook.Id + '\'';
        
        if(searchString!=null){
            qString+= ' and (Product__r.Name like \'%' + searchString + '%\' or Product__r.Description like \'%' + searchString + '%\')';
        }
        
                
        qString+= ' order by Product__r.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AvailableProducts = database.query(qString);
    }
    
    public void addToShoppingCart(){
    
        // This function runs when a user hits "select" button next to a product
    
        for(Price_Set_Item__c d : AvailableProducts){
            if((String)d.Id==toSelect){
                system.debug('Satya1:' +d); 
                system.debug('Satya2:' +d.product__c); 
                shoppingCart.add(new Quote_Line_Item__c(Quote__c=theQuote.Id, Unit_Price__c=d.List_Price__c, Product__c=d.Product__c, Description__c =d.Product__r.Description, Discount__c =0.0));
                break;
            }
        }
        
        updateAvailableList();  
    }
    

    public PageReference removeFromShoppingCart(){
    
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
    
        Integer count = 0;
    
        for(Quote_Line_Item__c d : shoppingCart){
            if((String)d.Id==toUnselect){
            
                if(d.Id!=null)
                    forDeletion.add(d);
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }
    
    public PageReference onSave(){
    
        // If previously selected products are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(shoppingCart.size()>0)
                upsert(shoppingCart);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
           
        // After save return the user to the Quote__c
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Quote__c   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }

}