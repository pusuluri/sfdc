public class PurchaseCartController {

    public static string QuoteId;
    public static string QuoteName;
    public Boolean multipleCurrencies {get; set;}
    public Quote__c theQuote {get;set;}
    public Price_Set__c theBook {get;set;}   
    public Quote_Line_Item__c[] shoppingCart {get;set;}
    
    private Boolean forcePricebookSelection = false;
    public static string MainId;
    public Price_Set_Item__c[] AvailableProducts {get;set;}
    
    public static List<ProductRelationshipWrapper> AllProducts;
    //public Set<Id> PriceSetItemProducts = new Set<Id>();

    public static List<Id> PriceSetItemProducts = new List<Id>();
    
      //constructor
      public PurchaseCartController (){    
      
        MainId=Apexpages.currentpage().getparameters().get('Id');
        theQuote = [select Id, name,Price_Set__c, Price_Set__r.Name from Quote__c where Id = :MainId limit 1];
         
        shoppingCart = [select Id,Discount__c, Product__c,Product__r.Name,Product__r.Description,Quantity__c, Total_Price__c, Unit_Price__c, Description__c from Quote_Line_Item__c where Quote__c=:theQuote.Id];

        // Check if Opp has a pricebook associated yet
        if(theQuote.Price_Set__c == null){
            Price_Set__c[] activepbs = [select Id, Name from Price_Set__c where Active__c = true limit 2];
            if(activepbs.size() == 2){
                forcePricebookSelection = true;
                theBook = new Price_Set__c();
            }
            else{
                theBook = activepbs[0];
            }
        }
        else{
            Price_Set__c[] activepbs1 = [select Id, Name from Price_Set__c where Active__c = true and id=:theQuote.Price_Set__c limit 1];
            theBook = activepbs1[0];
        }
        
         //get proposal name
         QuoteName=theQuote.name;         
         //proposal Id to show on header
         QuoteId=theQuote.Id;
         updateAvailableList();     
        //getAccountAssets();
      }      
    
    public static String getQuoteId(){
        return JSON.serialize(QuoteId);
    }
    
    public static String getQuoteName(){
        return JSON.serialize(QuoteName);
    }
    
    public static String getAccountAssets(){
    
        //System.debug(LoggingLevel.INFO, 'Ids ' + accountId + ' ' + configId);
        AllProducts = new List<ProductRelationshipWrapper>();
        MainId=Apexpages.currentpage().getparameters().get('Id');
        integer queryLimit = 50;
        string offset = '0';
        string soql = '';
        /*soql = 'select Id, name, SPSystems__Child__c,SPSystems__Parent__c from SPSystems__Product_Relationship__c';
       soql = soql + ' where SPSystems__Parent__c in ('+PriceSetItemProducts+')'+' LIMIT '+queryLimit+' OFFSET '+offset;
       List<SPSystems__Product_Relationship__c> accountAssets = database.query(soql);
       */
       
       List<SPSystems__Product_Relationship__c> accountAssets = [select Id, name, SPSystems__Child__c,SPSystems__Parent__c from SPSystems__Product_Relationship__c where SPSystems__Parent__c in :PriceSetItemProducts ];
       
       //get corresponding child assets of top level asset
       List<SPSystems__Product_Relationship__c> childAssets = [select Id, name, SPSystems__Child__c,SPSystems__Parent__c from SPSystems__Product_Relationship__c where SPSystems__Option_Code__c =: 'OPTION' and SPSystems__Parent__c in :PriceSetItemProducts ];
       
       
       System.debug(LoggingLevel.INFO, 'soql ' + soql);
       
       system.debug(LoggingLevel.INFO, 'AccountAssets list****' + accountAssets.size()); 
       system.debug(LoggingLevel.INFO, 'ChildAssets**' + childAssets.size()); 
       
       map<string, list<SPSystems__Product_Relationship__c>> parentChildAssetMap=new map<string, list<SPSystems__Product_Relationship__c>>();
       
       for(SPSystems__Product_Relationship__c childasset : childAssets){
           if(childasset.SPSystems__Option_Code__c=='OPTION'){
               if (parentChildAssetMap.containsKey(childasset.SPSystems__Parent__c)){
                   parentChildAssetMap.get(childasset.SPSystems__Parent__c).add(childasset);
               } else {
                   parentChildAssetMap.put(childasset.SPSystems__Parent__c, new  List <SPSystems__Product_Relationship__c> { childasset });
               }
           }
       }
       
       for(SPSystems__Product_Relationship__c asset : accountAssets){
           //if( asset.Apttus_Config2__LineType__c=='MAIN'){
               AllProducts.add(new ProductRelationshipWrapper(asset, parentChildAssetMap.get(asset.Id)));
           //}
       }
        
        
       system.debug(LoggingLevel.INFO, 'assets**' + AllProducts.size()); 
       return JSON.serialize(AllProducts);
   }
 
    public void updateAvailableList() {
    
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Price_Set__c, Active__c, Product__c, Product__r.Name, Product__r.Family, Product__r.IsActive, Product__r.Description, List_Price__c from Price_Set_Item__c where Active__c=true and Price_Set__c = \'' + theBook.Id + '\'';
        
        
        Set<Id> selectedEntries = new Set<Id>();
        //for(Quote_Line_Item__c d:shoppingCart){
        //    selectedEntries.add(d.Price_Set_Item__c);
        //}
        
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        }
        
        qString+= ' order by Product__r.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AvailableProducts = database.query(qString);
        
        for(Price_Set_Item__c setprodids:AvailableProducts){
            PriceSetItemProducts.add(setprodids.Product__c);
        }
              
    }
    
    public class ProductRelationshipWrapper{
          
        //parent assets
        public SPSystems__Product_Relationship__c grandparent {get;set;}
        //child assets
        public List<SPSystems__Product_Relationship__c> childsAssets {get; set;}
        public string gparentname {get; set;}
        public Boolean selected {get; set;}
        public Boolean IsaddedtoCart {get; set;}


        public ProductRelationshipWrapper(){
        }

        //constructor     
        public ProductRelationshipWrapper(SPSystems__Product_Relationship__c parentAsset, List<SPSystems__Product_Relationship__c> childAssetsNew){
           childsAssets= childAssetsNew;
           gparentname=parentAsset.name;
           grandparent= parentAsset;
           selected = false;
           
        }
    } 
    
}